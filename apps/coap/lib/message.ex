defmodule Coap.Message do

  defstruct version: 1,
            type: nil,
            code: nil,
            message_id: 0,
            token: <<>>,
            options: [],
            payload: <<>>

  def decode(packet) do
    <<
      version :: size(2),
      type :: size(2),
      tokenlength :: size(4),
      code :: binary - size(1),
      message_id :: size(16),
      data :: binary
    >> = packet

    <<tok :: binary - size(tokenlength),
      data :: binary>> = data

#    multiple instance of the same option can be include
#    by using a delta of zero.
    {opts, payload} = decode([], 0, data)

    %Coap.Message{
      version: version,
      type: Coap.Type.decode(type),
      code: Coap.Code.decode(code),
      message_id: message_id,
      token: tok,
      options: Enum.reverse(opts),
      payload: payload
    }
  end


  defp decode(opts, _delta, <<>>) do
    {opts, <<>>}
  end

  #   If present and of non-zero length, it is prefixed by a
  #   fixed, one-byte Payload Marker (0xFF), which indicates the end of
  #   options and the start of the payload.
  defp decode(opts, _delta, <<0xFF, payload :: binary>>) do
    {opts, payload}
  end

  defp decode(opts, _delta, <<del :: size(4), len :: size(4), rest :: binary>>) do
    {delta, rest} = Coap.Option.delta(del, rest)
    {length, rest} = Coap.Option.length(len, rest)
    {value, rest} = Coap.Option.value(length, rest)
    num = _delta + delta
    name = Coap.Option.name(num)
    decode([{name, value} | opts], num, rest)
  end


  def encode(msg = %Coap.Message{}) do
    <<msg.version :: size(2), Coap.Type.encode(msg.type) :: size(2), byte_size(msg.token) :: size(4)>> <> Coap.Code.encode(msg.code) <> <<msg.message_id :: size(16)>> <> msg.token <> encode_options(msg.options, 0) <> encode_payload(msg.payload)
  end

  defp encode_options([], _) do
    <<>>
  end

  defp encode_options([{name, value} | tail], delta) do
    {opt, d} = Coap.Option.encode(name, value, delta)
    opt <> encode_options(tail, d)
  end

  defp encode_payload(<<>>) do
    <<>>
  end

  defp encode_payload(payload) do
    <<0xFF>> <> payload
  end


end
