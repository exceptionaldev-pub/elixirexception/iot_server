defmodule Coap do
  def decode(packet) do
    Coap.Message.decode(packet)
  end

  def encode(msg) do
    Coap.Message.encode(msg)
  end
end


#defmodule Coap do
#  def decode do
#    packet = <<64, 1, 0, 123, 59, 101, 120, 97, 109, 112, 108, 101, 46, 99, 111, 109, 221, 1,
#      3, 97, 112, 112, 108, 105, 99, 97, 116, 105, 111, 110, 47, 106, 115, 111, 110,
#      255>>
#    Coap.Message.decode(packet)
#  end
#
#  def encode do
#    msg = %Coap.Message{
#      code: :get,
#      message_id: 123,
#      options: [uri_host: "example.com", accept: "application/json"],
#      payload: "",
#      token: "",
#      type: :confirmable,
#      version: 1
#    }
#
#
#    Coap.Message.encode(msg)
#  end
#end