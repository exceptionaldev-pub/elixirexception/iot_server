defmodule Coap.Type do

  def decode(type) do
    case type do
      0 -> :confirmable
      1 -> :non_confirmable
      2 -> :acknowledgement
      3 -> :reset
    end
  end

  def encode(type) do
    case type do
      :confirmable -> 0
      :non_confirmable -> 1
      :acknowledgement -> 2
      :reset -> 3
    end
  end

end
