-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 14, 2018 at 08:50 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Sensors`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id_m` int(11) NOT NULL,
  `id_s` int(11) NOT NULL,
  `pid` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id_m`, `id_s`, `pid`, `topic`, `message`) VALUES
                                                                         (1, 1, '#PID<0.328.0>', '/home/room1', 'It\'s hot :|'),
                                                                         (2, 1, '#PID<0.328.0>', '/home/room1', 'It\'s cold :|'),
                                                                         (3, 2, '#PID<0.328.0>', '/home/room2', 'The baby is awake :|'),
                                                                         (4, 2, '#PID<0.328.0>', '/home/room3', 'Refrigerator door is open'),
                                                                         (5, 3, '#PID<0.328.0>', '/home/door', 'mother comes!');

-- --------------------------------------------------------

--
-- Table structure for table `sensor_attr`
--

CREATE TABLE `sensor_attr` (
  `id_s` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sensor_attr`
--

INSERT INTO `sensor_attr` (`id_s`, `name`, `place`) VALUES
                                                           (1, 'temperature', '/home/room1'),
                                                           (2, 'buzzer', '/home/room2'),
                                                           (3, 'fingerprint', '/home/door');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_m`);

--
-- Indexes for table `sensor_attr`
--
ALTER TABLE `sensor_attr`
  ADD PRIMARY KEY (`id_s`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id_m` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sensor_attr`
--
ALTER TABLE `sensor_attr`
  MODIFY `id_s` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;