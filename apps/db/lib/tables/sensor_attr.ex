defmodule DB.Application.SensorAttr do
  use Ecto.Schema

@primary_key {:id, :integer, []}
  schema "sensor_attr" do
#    field :id, :integer
    field :name, :string
    field :place, :string

  end
  def changeset(struct, params \\ %{}) do
    struct
    |> Ecto.Changeset.cast(params, :id, :name, :place)
  end
end
