defmodule DB.Application.Message do
  use Ecto.Schema

@primary_key {:id, :integer, []}
  @foreign_key_type :id_s
  schema "message" do
#    field :id, :integer
    field :id_s, :integer
    field :pid, :string
    field :topic, :string
    field :message, :string
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> Ecto.Changeset.cast(params, :id, :id_s, :pid, :topic, :message)
  end
end
