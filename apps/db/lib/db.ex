defmodule DB do
  import Ecto.Query

  #  Add a new sensor (add/2)
  def add(name, place) do
    new_sensor = %DB.Application.SensorAttr{
      name: name,
      place: place
    }
    case DB.Repo.insert(new_sensor) do
      {:ok, sensor} -> IO.puts("Everything is Done!")
      {:error, sensor} -> IO.puts("Something went Wrong :( Try again!")
    end
  end

  #  Add a new message of a sensor (add/4)
  def add(id_sensor, pid, topic, message) do
    new_message = %DB.Application.Message{
      id_s: id_sensor,
      pid: Kernel.inspect(pid),
      topic: topic,
      message: message
    }
    case DB.Repo.insert(new_message) do
      {:ok, msg} -> IO.puts("Everything is Done!")
      {:error, msg} -> IO.puts("Something went Wrong :( Try again!")
    end
  end

  #  Get all message of a specific sensor (get_msg/1)
  def get_msg(id_sensor) do
    messages =
      DB.Application.Message
      |> Ecto.Query.where(id_s: ^id_sensor)
      |> DB.Repo.all
  end

  #  Get the messages of a specific sensor with given topic (get_msg/2)
  def get_msg(id_sensor, topic) do
    messages =
      DB.Application.Message
      |> Ecto.Query.where(id_s: ^id_sensor, topic: ^topic)
      |> DB.Repo.all
  end

  #  Get all attributes of all sensors (get_sensor/0)
  def get_sensor do
    sensors =
      DB.Application.SensorAttr
      |> DB.Repo.all
  end

  #  Get all attributes of one specific sensor (get_sensor/1)
  #  always return one record
  def get_sensor(id_sensor) do
    sensor =
      DB.Application.SensorAttr
      |> DB.Repo.get_by(id: id_sensor)
  end

end