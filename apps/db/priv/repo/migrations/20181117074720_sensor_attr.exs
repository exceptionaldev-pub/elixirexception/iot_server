# mix ecto.gen.migration sensor_attr -r DB.Repo
defmodule DB.Repo.Migrations.SensorAttr do
  use Ecto.Migration

  def change do
    create table(:sensor_attr) do
      add :name, :string
      add :place, :string
    end
  end
end
