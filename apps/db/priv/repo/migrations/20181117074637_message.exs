# mix ecto.gen.migration message -r DB.Repo
defmodule DB.Repo.Migrations.Message do
  use Ecto.Migration

  def change do
    create table(:message) do
      add :id_s, :integer
      add :pid, :string
      add :topic, :string
      add :message, :string
    end
  end
end
