# DB

**Storing all information about sensors and their messages.**

## Installation

First change `config/config.exs` with your own information
and then create the database and its tables with these following commands:

```elixir
cd apps/db 
mix ecto.create -r DB.Repo     # Create database
mix ecto.migrate -r DB.Repo    # Create tables
```


