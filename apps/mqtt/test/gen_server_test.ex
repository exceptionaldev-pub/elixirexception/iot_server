defmodule GenServerTest do
  use ExUnit.Case
  doctest GenServerTesting

  setup do
    {:ok,server_pid} = GenServerTesting.start_link("foo")
    {:ok,server: server_pid}
  end

  test "unlock success test", %{server: pid} do
    assert :ok == GenServerTesting.unlock(pid,"foo")
  end

  test "unlock failure  test", %{server: pid} do
    assert {:error,"wrongpassword"} == GenServerTesting.unlock(pid,"bar")
  end

  test "reset failure error" ,%{server: pid} do
    assert {:error,"wrongpassword"} == GenServerTesting.reset(pid,{"hello","bar"})
  end

  test "reset success test" ,%{server: pid} do
    assert :ok == GenServerTesting.reset(pid,{"foo","bar"})
  end

#  use ExUnit.Case, async: true
#
#  setup do
#    gst = start_supervised!(GenServerTesting)
#    %{gst: gst}
#  end
#
#  test "spawns buckets", %{gst: gst} do
#    assert GenServerTesting.lookup(gst, "shopping") == :error
#
#    GenServerTesting.create(gst, "shopping")
#    assert {:ok, bucket} = GenServerTesting.lookup(gst, "shopping")
#
##    KV.Bucket.put(bucket, "milk", 1)
##    assert KV.Bucket.get(bucket, "milk") == 1
#  end
#
#  test "removes buckets on exit", %{gst: gst} do
#    GenServerTesting.create(gst, "shopping")
#    {:ok, bucket} = GenServerTesting.lookup(gst, "shopping")
##    Agent.stop(bucket)
#    assert GenServerTesting.lookup(gst, "shopping") == :error
#  end

end


