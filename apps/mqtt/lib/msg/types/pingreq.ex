defmodule Pingreq do
  def start(
        <<
          12 :: 4,
          0 :: 4,
          0 :: 8
        >>,
        socket
      ) do
    PingResp.start(socket)

  end

end