defmodule Subscribe do
  def start(
        <<
          8 :: 4,
          2 :: 4,
          message_body :: binary
        >>,
        socket
      )
    do

    rms = Utils.remaining_length(message_body)
    {remaining_length, packet_identifier, payload} = body_parser(rms, message_body)
    topic_lists = parser(payload)
    qos_list = requested_qos(topic_lists)


    #save to database
    #The SUBACK Packet sent by the Server to the Client MUST contain a return code for each Topic
    #Filter/QoS pair. This return code MUST either show the maximum QoS that was granted for that
    #Subscription or indicate that the subscription failed [MQTT-3.8.4-5]. The Server might grant
    #a lower maximum QoS than the subscriber requested. The QoS of Payload Messages sent in response
    #to a Subscription MUST be the minimum of the QoS of the originally published message and the
    #maximum QoS granted by the Server. The server is permitted to send duplicate copies of a message
    #to a subscriber in the case where the original message was published with QoS 1 and the maximum
    #QoS granted was QoS 0

    Suback.start(packet_identifier, socket, qos_list)
  end

  def body_parser(rms, message_body) do
    <<
      remaining_length :: size(rms) - binary,
      packet_identifier :: size(2) - binary,
      payload :: binary
    >> = message_body
    {remaining_length, packet_identifier, payload}
  end

  def parser(payload) when byte_size(payload) != 0 do
    <<
      x :: size(16),
      topic_name :: size(x) - binary,
      requested_qos :: size(8),
      payload :: binary
    >> = payload
    [{topic_name, requested_qos}] ++ parser(payload)
  end

  def parser(payload)do
    []
  end

  def requested_qos(topic_lists) when length(topic_lists) != 0 do
    var = Enum.find_value(topic_lists, fn {topic, qos} -> qos end)
    topic_lists = List.delete_at(topic_lists, 0)
    [var] ++ requested_qos(topic_lists)
  end

  def requested_qos(topic_lists)do
    []
  end
end
