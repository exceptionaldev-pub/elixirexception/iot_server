defmodule PingResp do
  @moduledoc false
  def start(socket) do

    Network.Handler.sender(
      <<
        Utils.msg_type_to_binary(:pingresp) :: 4,
        0 :: 4,
        0 :: 8
      >>,
      socket
    )
  end
end
