defmodule Mqtt.Connect do

  defstruct client_id: "",
            username: "",
            password: "",
            keep_alive: :infinity,
            keep_alive_server_ms: :infinity,
            last_will: false,
            will_qos: :fire_and_forget,
            will_retain: false,
            will_topic: "",
            will_message: "",
            clean_session: true

  def start(
        <<
          0,
          4,
          "MQTT",
          protocol_level :: size(1) - binary,
          connect_flags :: size(1) - binary,
          keep_alive :: size(16),
          payload :: binary
        >>,
        socket
      )
    do

    #parsing the flags byte
    {username_flag, password_flag, will_retain, will_qos, will_flag, clean_session}
    = connect_flag_parser(connect_flags)

    #usage of parser
    {client_id, payload} = parser(1, payload)
    {will_topic, payload} = parser(will_flag, payload)
    {will_message, payload} = parser(will_flag, payload)
    {username, payload} = parser(username_flag, payload)
    {password, payload} = parser(password_flag, payload)

    session_present = if clean_session == 1 do
      1
    else
      if true do
        1
      else
        0
      end
    end

    Mqtt.Connack.start(protocol_level, session_present, :ok, socket)
    new(
      client_id,
      username,
      password,
      clean_session,
      keep_alive,
      will_qos,
      will_retain,
      will_topic,
      will_message
    )
  end

  defp parser(1, payload) do
    <<
      x :: size(16),
      var :: size(x) - binary,
      payload :: binary
    >> = payload
    {var, payload}
  end

  defp parser(0, payload) do
    {nil, payload}
  end

  defp connect_flag_parser(
         <<
           username_flag :: size(1),
           password_flag :: size(1),
           will_retain :: size(1),
           will_qos :: size(2),
           will_flag :: size(1),
           clean_session :: size(1),
           _ :: size(1)
         >> = connect_flags
       ) do
    {username_flag, password_flag, will_retain, will_qos, will_flag, clean_session}
  end

  defp new(
         client_id,
         username,
         password,
         clean_session,
         keep_alive,
         will_qos,
         will_retain,
         will_topic,
         will_message
       ) do

    %__MODULE__{
      client_id: client_id,
      username: username,
      password: password,
      keep_alive: keep_alive,
      keep_alive_server_ms: :infinity,
      last_will: false,
      will_qos: will_qos,
      will_retain: will_retain,
      will_topic: will_topic,
      will_message: will_message,
      clean_session: clean_session
    }
  end

end
