defmodule Suback do

  def start(packet_identifier, socket, qos_list) do

    var = packet_identifier <> return_codes(qos_list)
    remaining_length = byte_size(var)
    var = <<
            Utils.msg_type_to_binary(:suback) :: 4,
            0 :: 4,
          >> <> <<remaining_length>> <> var

    Network.Handler.sender(
      var,
      socket
    )

  end

  def return_codes(qos_list) when length(qos_list) != 0 do
    rc = case List.first(qos_list) do
      0 -> 0x00
      1 -> 0x01
      2 -> 0x02
      _ -> 0x80
    end
    qos_list = List.delete_at(qos_list, 0)
    <<rc>> <> return_codes(qos_list)
  end
  def return_codes(qos_list) do
    <<>>
  end
end
