defmodule Publish do

  # Publish
  def start(
        <<
          3 :: 4,
          dup :: size(1),
          qos :: size(2),
          retain :: size(1),
          message_body :: binary
        >>,
        socket
      ) do

    if qos > 2 do
      #shoutdown socket
      exit(:shutdown)
    end

    if retain == 1 do
      #Save topic and message and qos for future subscriber
    end

    if qos == 0 and retain == 1 do
      #discard any message previously retained for that topic. It SHOULD store the new QoS 0 message as the new retained
      #message for that topic,
    end

    #When sending a PUBLISH Packet to a Client the Server MUST set the RETAIN flag to 1 if a message is sent as a result
    #of a new subscription being made by a Client [MQTT-3.3.1-8]. It MUST set the RETAIN flag to 0 when a PUBLISH Packet
    #is sent to a Client because it matches an established subscription regardless of how the flag was set in the message
    #it received [MQTT-3.3.1-9].

    rms = Utils.remaining_length(message_body)

    #    IO.puts(rms)
    {topic_name, message, packet_identifier} = body_parser(qos, rms, message_body)

    if retain == 1 and byte_size(message) == 0 do
      #processed as normal by the Server and sent to Clients
      #same topic name MUST be removed and any future subscribers for the topic will not receive a retained message
      #A zero byte retained message MUST NOT be stored as a retained message on the Server
    end

    respond(qos, packet_identifier, socket)

  end

  def respond(0, "", socket) do
  end

  def respond(1, packet_identifier, socket) do
    Mqtt.Puback.start(packet_identifier, socket)
    {:ok}
  end

  def respond(2, packet_identifier, socket) do
    Mqtt.Pubrec.start(packet_identifier, socket)
    {:ok}
  end

  def body_parser(0, rms, message_body) do
    IO.puts(rms)
    <<
      remaining_length :: size(rms) - binary,
      topic_size :: size(16),
      topic_name :: size(topic_size) - binary,
      message :: binary
    >> = message_body
    {topic_name, message, ""}
  end

  def body_parser(qos, rms, message_body) do
    <<
      remaining_length :: size(rms) - binary,
      topic_size :: size(16),
      topic_name :: size(topic_size) - binary,
      packet_identifier :: size(2) - binary,
      message :: binary
    >> = message_body
    {topic_name, message, packet_identifier}
  end


end
