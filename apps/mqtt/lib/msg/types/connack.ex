defmodule Mqtt.Connack do

  def start(protocol_level, session_present, connect_return_atom, socket) do

    Network.Handler.sender(
      <<
        Utils.msg_type_to_binary(:connack) :: 4,
        0 :: 4,
        0x02
      >> <> connect_acknowledge_flags(session_present) <> <<conn_ack_status(connect_return_atom)>>,
      socket
    )

    #    <<0 :: size(1), 0 :: size(1), 1 :: size(1), 0 :: size(1), 0 :: size(1), 0 :: size(1), 0 :: size(1), 0 :: size(1),>>
  end

  def connect_acknowledge_flags 0 do
    <<0>>
  end

  def connect_acknowledge_flags 1 do
    <<1>>
  end

  def conn_ack_status(atom) do
    result = case atom do
      :ok -> 0
      :unaccaptable_protocol_version -> 1
      :identifier_rejected -> 2
      :server_unavailable -> 3
      :bad_user -> 4
      :not_authorized -> 5
    end
  end
end
