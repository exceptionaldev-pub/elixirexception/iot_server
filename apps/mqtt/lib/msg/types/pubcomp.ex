defmodule Pubcomp do

  def start(packet_identifier, socket) do
    Network.Handler.sender(
      <<
        Utils.msg_type_to_binary(:pubcomp) :: 4,
        0 :: 4,
        0x02
      >> <> packet_identifier,
      socket
    )

  end

end
