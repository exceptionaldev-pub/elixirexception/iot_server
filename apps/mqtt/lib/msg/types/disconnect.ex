defmodule Disconnect do

  def start(
        <<
          14 :: 4,
          0 :: 4,
          0 :: 8
        >>,
        socket
      ) do
    IO.puts("Disconnect")
  end

end
