defmodule Unsubscribe do
  def start(
        <<
          10 :: 4,
          2 :: 4,
          message_body :: binary
        >>,
        socket
      )
    do

    rms = Utils.remaining_length(message_body)
    {remaining_length, packet_identifier, payload} = body_parser(rms, message_body)
    topic_lists = parser(payload)
    IO.inspect(topic_lists)
    Unsuback.start(packet_identifier, socket)
  end

  def body_parser(rms, message_body) do
    <<
      remaining_length :: size(rms) - binary,
      packet_identifier :: size(2) - binary,
      payload :: binary
    >> = message_body
    {remaining_length, packet_identifier, payload}
  end

  def parser(payload) when byte_size(payload) != 0 do
    <<
      x :: size(16),
      topic_name :: size(x) - binary,
      payload :: binary
    >> = payload
    [topic_name] ++ parser(payload)
  end

  def parser(payload)do
    []
  end


end
