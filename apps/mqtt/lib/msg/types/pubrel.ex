defmodule Pubrel do
  def start(
        <<
          fixed_header :: 16,
          packet_identifier :: binary
        >>,
        socket
      ) do
    Pubcomp.start(packet_identifier, socket)
  end

end
