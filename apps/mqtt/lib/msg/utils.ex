defmodule Utils do

  def msg_type_to_binary(atom) do
    case atom do
      :reserved -> 0
      :connect -> 1
      :connack -> 2
      :publish -> 3
      :puback -> 4
      :pubrec -> 5
      :pubrel -> 6
      :pubcomp -> 7
      :subscribe -> 8
      :suback -> 9
      :unsubscribe -> 10
      :unsuback -> 11
      :pingreq -> 12
      :pingresp -> 13
      :disconnect -> 14
    end
  end

  def remaining_length(message_body) do
    sb = byte_size(message_body)
    rl = cond do
      sb >= 0 and sb <= 127 -> 1
      sb >= 128 and sb <= 16383 -> 2
      sb >= 16384 and sb <= 2097151 -> 3
      sb >= 2097152 and sb <= 268435455 -> 4
      true -> 1
    end
  end

end
