defmodule Sandbox do
  def parser(payload) when byte_size(payload) != 0 do
    <<
      x :: size(16),
      topic_name :: size(x) - binary,
      requested_qos :: size(8),
      payload :: binary
    >> = payload
    parser(payload) ++ [{topic_name, requested_qos}]
  end
  def parser(payload)do
    []
  end
  def printer (list) do
    IO.inspect(list)
  end
end
