defmodule Mqtt.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: :mqtt_supervisor)
  end

  def start_socket(name) do
    # And we use `start_child/2` to start a new Chat.Server process
    Supervisor.start_child(:mqtt_supervisor, [name])
  end

  def init(_) do
    children = [
      worker(Mqtt.Incoming, [])
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end
