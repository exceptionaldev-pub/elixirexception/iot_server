defmodule Mqtt.Incoming do

  def begin(packet, socket) do
    <<
      fixed_header :: size(1) - binary,
      remaining_length :: size(1) - binary,
      rest :: binary
    >> = packet

    mod = control_packet_parser(fixed_header)

    {dup, qos, retain} = if mod == :publish do
      publish_flag_parser(fixed_header)
    else
      {0, 0, 0}
    end

    pi = does_it_have_PI(mod)

    pi = if qos > 0 && mod == :publish do
      true
    else
      false
    end
    IO.inspect(mod)
    var = case mod do
      :connect -> Mqtt.Connect.start(rest, socket)
      :publish -> Publish.start(packet, socket)
      :pubrel -> Pubrel.start(packet, socket)
      :subscribe -> Subscribe.start(packet, socket)
      :unsubscribe -> Unsubscribe.start(packet, socket)
      :pingreq -> Pingreq.start(packet, socket)
      :disconnect -> Disconnect.start(packet, socket)
      _ -> IO.inspect(mod)
    end

  end

  defp control_packet_parser(fixed_header) do
    <<fixed_header :: size(4), remaining :: size(4)>> = fixed_header
    control_packet_type = case fixed_header do
      0 -> :reserved
      1 -> :connect
      2 -> :connack
      3 -> :publish
      4 -> :puback
      5 -> :pubrec
      6 -> :pubrel
      7 -> :pubcom
      8 -> :subscribe
      9 -> :suback
      10 -> :unsubscribe
      11 -> :unsuback
      12 -> :pingreq
      13 -> :pingresp
      14 -> :disconnect
      15 -> :reserved
    end
  end

  defp packet_identifier(packet) do
    <<
      pi_msb :: size(1) - binary,
      pi_lsb :: size(1) - binary,
      packet :: binary
    >> = packet
  end

  defp publish_flag_parser(fixed_header) do
    <<
      fixed_header :: size(4),
      dup :: size(1),
      qos :: size(2),
      retain :: size(1),
    >> = fixed_header
    {dup, qos, retain}
  end

  def does_it_have_PI(mod) do
    case mod
      do
      :puback -> true
      :pubrec -> true
      :pubrel -> true
      :pubcom -> true
      :subscribe -> true
      :suback -> true
      :unsubscribe -> true
      :unsuback -> true
      _ -> false
    end
  end
end
