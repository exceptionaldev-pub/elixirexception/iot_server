defmodule Mqtt.Application do
  use Application

  def start(_type, _args) do
    Mqtt.Supervisor.start_link
  end
end
