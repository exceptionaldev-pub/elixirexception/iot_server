defmodule Actor.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec
    # List all child processes to be supervised
    children = [
    Actor.Repo,
      # Starts a worker by calling: Actor.Worker.start_link(arg)
      # {Actor.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Actor.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
