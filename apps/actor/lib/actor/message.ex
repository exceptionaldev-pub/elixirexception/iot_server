defmodule Actor.Message do
  use Ecto.Schema
  @primary_key {:id_message, :id, autogenerate: true}

  schema "Messages" do
    field :id_user, :integer
    field :id_topic, :integer
    field :message, :string
    field :time_add, :naive_datetime



  end
end
