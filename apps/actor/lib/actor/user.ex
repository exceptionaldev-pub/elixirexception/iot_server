defmodule Actor.User do
  use Ecto.Schema
  @primary_key {:id_user, :id, autogenerate: true}
  schema "Users" do
    field :username, :string
    field :passwd, :string
    field :active, :boolean
    field :time_add, :naive_datetime

  end


end

