defmodule Actor.Topic do
  use Ecto.Schema
  @primary_key {:id_topic, :id, autogenerate: true}

  schema "Topics" do
    field :time_add, :naive_datetime
    field :topic, :string
    field :valid, :boolean
    field :active, :boolean

  end

end

