defmodule Network.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(Network.Worker, []),
    ]

    opts = [strategy: :one_for_one, name: Spvisor]
    Supervisor.start_link(children, opts)
  end
end
