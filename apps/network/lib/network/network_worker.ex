defmodule Network.Worker do
  def start_link do
    opts = [port: 1883]
    {:ok, _} = :ranch.start_listener(:AsyncAck, 10, :ranch_tcp, opts, Network.Handler, [])
  end
end
